How to use Instagram Application

1. Set up your account and profile
If you have not downloaded the Instagram app, you can grab it from the App Store, Google Play Store, or Microsoft Store.

When you create your Instagram account on the mobile app, the app will guide you through a few basic steps for getting set up.
Here are two things to take note of:

Profile photo
Your Instagram profile photo will be displayed as a circle. If you are using your business logo, be sure to keep it in the center of your image.
Also, as your profile image will look relatively small in the app, you might want to use a prominent logo mark, instead of a logo with text.

Profile information
The app will not prompt you to fill out your profile information but it’ll be great to do so. To fill out your profile information, go to your profile in the app and tap on “Edit Profile”. The two fields to fill out is your website and your bio. 
If you would like to change your Instagram username (i.e. @username), you can also change it here.
